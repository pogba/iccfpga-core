set_property INTERNAL_VREF 0.675 [get_iobanks 34]

create_generated_clock -name cpu_clk [get_pins design_iccfpga_i/clk_wiz_0/inst/mmcm_adv_inst/CLKOUT0]
create_clock -period 10.000 -name clk -waveform {0.000 5.000} [get_ports clk]
create_clock -name SWCLK -period 100 [get_ports SWCLK]

set_clock_groups -name clock_groups -asynchronous -group [get_clocks [get_clocks -of_objects [get_pins design_iccfpga_i/clk_wiz_0/inst/mmcm_adv_inst/CLKOUT0]]] -group [get_clocks [get_clocks -of_objects [get_pins design_iccfpga_i/clk_wiz_0/inst/mmcm_adv_inst/CLKOUT2]]] -group [get_clocks SWCLK]

set_input_delay -clock [get_clocks cpu_clk] -add_delay 0.500 [get_ports reset*]

set_property PACKAGE_PIN H17 [get_ports SWDRESET]
set_property PACKAGE_PIN G16 [get_ports SWCLK]
set_property PACKAGE_PIN K14 [get_ports SWDIO]
#set_property BOARD_PIN {reset} [get_ports ext_reset_in]
set_property PACKAGE_PIN C18 [get_ports reset]
set_property PACKAGE_PIN J14 [get_ports iic_scl_io]
set_property PACKAGE_PIN J13 [get_ports iic_sda_io]
set_property PACKAGE_PIN R14 [get_ports TDO]
set_property PACKAGE_PIN T14 [get_ports TDI]
set_property PACKAGE_PIN R16 [get_ports nTRST]
set_property PACKAGE_PIN F14 [get_ports sys_clock]

set_property PACKAGE_PIN V16 [get_ports spi_miso0]
set_property PACKAGE_PIN U18 [get_ports spi_mosi0]
set_property PACKAGE_PIN U16 [get_ports spi_sck0]
set_property PACKAGE_PIN P13 [get_ports spi_ss0]
set_property PACKAGE_PIN R13 [get_ports spi_ss1]
set_property PACKAGE_PIN V14 [get_ports spi_miso1]
set_property PACKAGE_PIN T15 [get_ports spi_sck1]
set_property PACKAGE_PIN H16 [get_ports spi_mosi1]
set_property PACKAGE_PIN U15 [get_ports w5500_rstn]
set_property PACKAGE_PIN U17 [get_ports w5500_intn]

set_property IOSTANDARD LVCMOS33 [get_ports sys_clock]
set_property IOSTANDARD LVCMOS33 [get_ports spi_mosi0]
set_property IOSTANDARD LVCMOS33 [get_ports spi_miso0]
set_property IOSTANDARD LVCMOS33 [get_ports spi_sck0]
set_property IOSTANDARD LVCMOS33 [get_ports spi_ss0]
set_property IOSTANDARD LVCMOS33 [get_ports spi_mosi1]
set_property IOSTANDARD LVCMOS33 [get_ports spi_miso1]
set_property IOSTANDARD LVCMOS33 [get_ports spi_sck1]
set_property IOSTANDARD LVCMOS33 [get_ports spi_ss1]

set_property IOSTANDARD LVCMOS33 [get_ports w5500_rstn]
set_property IOSTANDARD LVCMOS33 [get_ports w5500_intn]


set_property PULLUP true [get_ports w5500_rstn]
set_property PULLUP true [get_ports w5500_intn]

set_property PULLUP true [get_ports spi_ss0]
set_property PULLUP true [get_ports spi_ss1]

set_property PULLUP true [get_ports SWDRESET]
set_property PULLUP true [get_ports SWDIO]
set_property PULLUP true [get_ports SWCLK]
set_property PULLUP true [get_ports nTRST]
set_property PULLDOWN true [get_ports TDI]

set_property IOSTANDARD LVCMOS33 [get_ports nTRST]
set_property IOSTANDARD LVCMOS33 [get_ports TDI]
set_property IOSTANDARD LVCMOS33 [get_ports TDO]
set_property IOSTANDARD LVCMOS33 [get_ports reset]
set_property IOSTANDARD LVCMOS33 [get_ports iic_scl_io]
set_property IOSTANDARD LVCMOS33 [get_ports iic_sda_io]
set_property IOSTANDARD LVCMOS33 [get_ports SWDRESET]
set_property IOSTANDARD LVCMOS33 [get_ports SWCLK]
set_property IOSTANDARD LVCMOS33 [get_ports SWDIO]

set_property CFGBVS VCCO [current_design]
set_property CONFIG_VOLTAGE 3.3 [current_design]

set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4 [current_design]


#encryption settings
#set_property BITSTREAM.ENCRYPTION.ENCRYPT YES [current_design]
##set_property BITSTREAM.ENCRYPTION.ENCRYPTKEYSELECT BBRAM [current_design]
#set_property BITSTREAM.ENCRYPTION.ENCRYPTKEYSELECT eFUSE [current_design]
#set_property BITSTREAM.ENCRYPTION.KEY0 256'h78214125442A472D4B614E645267556B58703273357638792F423F4528482B4D [current_design]

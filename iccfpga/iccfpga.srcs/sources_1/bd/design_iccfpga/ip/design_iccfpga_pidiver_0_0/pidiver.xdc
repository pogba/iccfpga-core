
create_clock -period 5.376 -name clk_fast -waveform {0.000 2.688} [get_ports clk_fast]
create_clock -period 10.000 -name clk_slow -waveform {0.000 5.000} [get_ports clk_slow]
set_clock_groups -name clock_groups_pidiver -asynchronous -group [get_clocks clk_fast] -group [get_clocks clk_slow]

#set_multicycle_path -from [get_pins -hierarchical *curl_state*] -to [get_pins -hierarchical *mid*] 2

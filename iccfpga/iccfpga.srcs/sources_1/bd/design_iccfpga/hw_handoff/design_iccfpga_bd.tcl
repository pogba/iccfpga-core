
################################################################
# This is a generated script based on design: design_iccfpga
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

namespace eval _tcl {
proc get_script_folder {} {
   set script_path [file normalize [info script]]
   set script_folder [file dirname $script_path]
   return $script_folder
}
}
variable script_folder
set script_folder [_tcl::get_script_folder]

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2018.2
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   catch {common::send_msg_id "BD_TCL-109" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."}

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source design_iccfpga_script.tcl

# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./myproj/project_1.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project project_1 myproj -part xc7s50csga324-1
   set_property BOARD_PART digilentinc.com:arty-s7-50:part0:1.0 [current_project]
}


# CHANGE DESIGN NAME HERE
variable design_name
set design_name design_iccfpga

# If you do not already have an existing IP Integrator design open,
# you can create a design using the following command:
#    create_bd_design $design_name

# Creating design if needed
set errMsg ""
set nRet 0

set cur_design [current_bd_design -quiet]
set list_cells [get_bd_cells -quiet]

if { ${design_name} eq "" } {
   # USE CASES:
   #    1) Design_name not set

   set errMsg "Please set the variable <design_name> to a non-empty value."
   set nRet 1

} elseif { ${cur_design} ne "" && ${list_cells} eq "" } {
   # USE CASES:
   #    2): Current design opened AND is empty AND names same.
   #    3): Current design opened AND is empty AND names diff; design_name NOT in project.
   #    4): Current design opened AND is empty AND names diff; design_name exists in project.

   if { $cur_design ne $design_name } {
      common::send_msg_id "BD_TCL-001" "INFO" "Changing value of <design_name> from <$design_name> to <$cur_design> since current design is empty."
      set design_name [get_property NAME $cur_design]
   }
   common::send_msg_id "BD_TCL-002" "INFO" "Constructing design in IPI design <$cur_design>..."

} elseif { ${cur_design} ne "" && $list_cells ne "" && $cur_design eq $design_name } {
   # USE CASES:
   #    5) Current design opened AND has components AND same names.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 1
} elseif { [get_files -quiet ${design_name}.bd] ne "" } {
   # USE CASES: 
   #    6) Current opened design, has components, but diff names, design_name exists in project.
   #    7) No opened design, design_name exists in project.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 2

} else {
   # USE CASES:
   #    8) No opened design, design_name not in project.
   #    9) Current opened design, has components, but diff names, design_name not in project.

   common::send_msg_id "BD_TCL-003" "INFO" "Currently there is no design <$design_name> in project, so creating one..."

   create_bd_design $design_name

   common::send_msg_id "BD_TCL-004" "INFO" "Making design <$design_name> as current_bd_design."
   current_bd_design $design_name

}

common::send_msg_id "BD_TCL-005" "INFO" "Currently the variable <design_name> is equal to \"$design_name\"."

if { $nRet != 0 } {
   catch {common::send_msg_id "BD_TCL-114" "ERROR" $errMsg}
   return $nRet
}

##################################################################
# DESIGN PROCs
##################################################################



# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder
  variable design_name

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports
  set led_4bits [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:gpio_rtl:1.0 led_4bits ]
  set push_buttons_4bits [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:gpio_rtl:1.0 push_buttons_4bits ]
  set usb_uart [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:uart_rtl:1.0 usb_uart ]

  # Create ports
  set SWCLK [ create_bd_port -dir I -type clk SWCLK ]
  set SWDIO [ create_bd_port -dir IO SWDIO ]
  set SWDRESET [ create_bd_port -dir I SWDRESET ]
  set TDI [ create_bd_port -dir I TDI ]
  set TDO [ create_bd_port -dir O -from 0 -to 0 TDO ]
  set iic_scl_io [ create_bd_port -dir IO iic_scl_io ]
  set iic_sda_io [ create_bd_port -dir IO iic_sda_io ]
  set nTRST [ create_bd_port -dir I nTRST ]
  set reset [ create_bd_port -dir I reset ]
  set spi_miso0 [ create_bd_port -dir IO spi_miso0 ]
  set spi_miso1 [ create_bd_port -dir IO spi_miso1 ]
  set spi_mosi0 [ create_bd_port -dir IO spi_mosi0 ]
  set spi_mosi1 [ create_bd_port -dir IO spi_mosi1 ]
  set spi_sck0 [ create_bd_port -dir IO spi_sck0 ]
  set spi_sck1 [ create_bd_port -dir IO spi_sck1 ]
  set spi_ss0 [ create_bd_port -dir O -from 0 -to 0 spi_ss0 ]
  set spi_ss1 [ create_bd_port -dir IO spi_ss1 ]
  set sys_clock [ create_bd_port -dir I -type clk sys_clock ]
  set w5500_intn [ create_bd_port -dir I w5500_intn ]
  set w5500_rstn [ create_bd_port -dir O -from 0 -to 0 w5500_rstn ]

  # Create instance: CORTEXM1_AXI_0, and set properties
  set CORTEXM1_AXI_0 [ create_bd_cell -type ip -vlnv arm.com:CortexM:CORTEXM1_AXI:1.1 CORTEXM1_AXI_0 ]
  set_property -dict [ list \
   CONFIG.DTCM_SIZE {"1000"} \
   CONFIG.ITCM_INIT_FILE {iccfpga.hex} \
   CONFIG.ITCM_INIT_RAM {true} \
   CONFIG.ITCM_SIZE {"1000"} \
 ] $CORTEXM1_AXI_0

  # Create instance: SWD_Two_Wire_0, and set properties
  set SWD_Two_Wire_0 [ create_bd_cell -type ip -vlnv user.org:user:SWD_Two_Wire:1.0 SWD_Two_Wire_0 ]
  set_property -dict [ list \
   CONFIG.LOCK {"0"} \
 ] $SWD_Two_Wire_0

  # Create instance: axi2lb_v1_0_0, and set properties
  set axi2lb_v1_0_0 [ create_bd_cell -type ip -vlnv user.org:user:axi2lb_v1_0:1.1 axi2lb_v1_0_0 ]

  # Create instance: axi2lb_v1_0_1, and set properties
  set axi2lb_v1_0_1 [ create_bd_cell -type ip -vlnv user.org:user:axi2lb_v1_0:1.1 axi2lb_v1_0_1 ]

  # Create instance: axi_bram_ctrl_0, and set properties
  set axi_bram_ctrl_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_bram_ctrl:4.0 axi_bram_ctrl_0 ]
  set_property -dict [ list \
   CONFIG.SINGLE_PORT_BRAM {1} \
 ] $axi_bram_ctrl_0

  # Create instance: axi_gpio_0, and set properties
  set axi_gpio_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio:2.0 axi_gpio_0 ]
  set_property -dict [ list \
   CONFIG.C_ALL_INPUTS_2 {1} \
   CONFIG.C_ALL_OUTPUTS {1} \
   CONFIG.C_DOUT_DEFAULT {0x0000002a} \
   CONFIG.C_GPIO2_WIDTH {8} \
   CONFIG.C_GPIO_WIDTH {8} \
   CONFIG.C_IS_DUAL {1} \
 ] $axi_gpio_0

  # Create instance: axi_gpio_1, and set properties
  set axi_gpio_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio:2.0 axi_gpio_1 ]
  set_property -dict [ list \
   CONFIG.C_ALL_INPUTS_2 {1} \
   CONFIG.C_GPIO2_WIDTH {4} \
   CONFIG.C_GPIO_WIDTH {4} \
   CONFIG.C_IS_DUAL {1} \
   CONFIG.GPIO2_BOARD_INTERFACE {push_buttons_4bits} \
   CONFIG.GPIO_BOARD_INTERFACE {led_4bits} \
   CONFIG.USE_BOARD_FLOW {true} \
 ] $axi_gpio_1

  # Create instance: axi_interconnect_0, and set properties
  set axi_interconnect_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 axi_interconnect_0 ]
  set_property -dict [ list \
   CONFIG.ENABLE_ADVANCED_OPTIONS {1} \
   CONFIG.M01_HAS_REGSLICE {0} \
   CONFIG.M02_HAS_REGSLICE {3} \
   CONFIG.M03_HAS_REGSLICE {3} \
   CONFIG.M04_HAS_REGSLICE {0} \
   CONFIG.NUM_MI {9} \
   CONFIG.S00_HAS_REGSLICE {3} \
 ] $axi_interconnect_0

  # Create instance: axi_quad_spi_0, and set properties
  set axi_quad_spi_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_quad_spi:3.2 axi_quad_spi_0 ]
  set_property -dict [ list \
   CONFIG.C_NUM_SS_BITS {1} \
   CONFIG.C_SCK_RATIO {8} \
   CONFIG.C_USE_STARTUP {0} \
 ] $axi_quad_spi_0

  # Create instance: axi_quad_spi_1, and set properties
  set axi_quad_spi_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_quad_spi:3.2 axi_quad_spi_1 ]
  set_property -dict [ list \
   CONFIG.C_SCK_RATIO {8} \
   CONFIG.C_USE_STARTUP {0} \
   CONFIG.QSPI_BOARD_INTERFACE {Custom} \
   CONFIG.USE_BOARD_FLOW {true} \
 ] $axi_quad_spi_1

  # Create instance: axi_uartlite_0, and set properties
  set axi_uartlite_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_uartlite:2.0 axi_uartlite_0 ]
  set_property -dict [ list \
   CONFIG.C_BAUDRATE {115200} \
   CONFIG.UARTLITE_BOARD_INTERFACE {usb_uart} \
   CONFIG.USE_BOARD_FLOW {true} \
 ] $axi_uartlite_0

  # Create instance: blk_mem_gen_0, and set properties
  set blk_mem_gen_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.4 blk_mem_gen_0 ]
  set_property -dict [ list \
   CONFIG.Enable_B {Use_ENB_Pin} \
   CONFIG.Memory_Type {True_Dual_Port_RAM} \
   CONFIG.Port_B_Clock {100} \
   CONFIG.Port_B_Enable_Rate {100} \
   CONFIG.Port_B_Write_Rate {50} \
   CONFIG.Use_RSTB_Pin {true} \
 ] $blk_mem_gen_0

  # Create instance: clk_wiz_0, and set properties
  set clk_wiz_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:clk_wiz:6.0 clk_wiz_0 ]
  set_property -dict [ list \
   CONFIG.CLKIN1_JITTER_PS {833.33} \
   CONFIG.CLKOUT1_DRIVES {BUFGCE} \
   CONFIG.CLKOUT1_JITTER {479.872} \
   CONFIG.CLKOUT1_PHASE_ERROR {668.310} \
   CONFIG.CLKOUT2_DRIVES {BUFGCE} \
   CONFIG.CLKOUT2_JITTER {522.315} \
   CONFIG.CLKOUT2_PHASE_ERROR {668.310} \
   CONFIG.CLKOUT2_REQUESTED_OUT_FREQ {50} \
   CONFIG.CLKOUT2_USED {true} \
   CONFIG.CLKOUT3_DRIVES {BUFGCE} \
   CONFIG.CLKOUT3_JITTER {445.229} \
   CONFIG.CLKOUT3_PHASE_ERROR {668.310} \
   CONFIG.CLKOUT3_REQUESTED_OUT_FREQ {187.5} \
   CONFIG.CLKOUT3_USED {true} \
   CONFIG.CLKOUT4_DRIVES {BUFGCE} \
   CONFIG.CLKOUT5_DRIVES {BUFGCE} \
   CONFIG.CLKOUT6_DRIVES {BUFGCE} \
   CONFIG.CLKOUT7_DRIVES {BUFGCE} \
   CONFIG.CLK_IN1_BOARD_INTERFACE {sys_clock} \
   CONFIG.FEEDBACK_SOURCE {FDBK_AUTO} \
   CONFIG.MMCM_CLKFBOUT_MULT_F {62.500} \
   CONFIG.MMCM_CLKIN1_PERIOD {83.333} \
   CONFIG.MMCM_CLKIN2_PERIOD {10.0} \
   CONFIG.MMCM_CLKOUT0_DIVIDE_F {7.500} \
   CONFIG.MMCM_CLKOUT1_DIVIDE {15} \
   CONFIG.MMCM_CLKOUT2_DIVIDE {4} \
   CONFIG.MMCM_DIVCLK_DIVIDE {1} \
   CONFIG.NUM_OUT_CLKS {3} \
   CONFIG.USE_RESET {false} \
   CONFIG.USE_SAFE_CLOCK_STARTUP {true} \
 ] $clk_wiz_0

  # Create instance: converter_0, and set properties
  set converter_0 [ create_bd_cell -type ip -vlnv user.org:user:converter:1.0 converter_0 ]

  # Create instance: i2c_io_0, and set properties
  set i2c_io_0 [ create_bd_cell -type ip -vlnv user.org:user:i2c_io:1.0 i2c_io_0 ]

  # Create instance: iobuffer_0, and set properties
  set iobuffer_0 [ create_bd_cell -type ip -vlnv user.org:user:iobuffer:1.0 iobuffer_0 ]

  # Create instance: iobuffer_1, and set properties
  set iobuffer_1 [ create_bd_cell -type ip -vlnv user.org:user:iobuffer:1.0 iobuffer_1 ]

  # Create instance: iobuffer_2, and set properties
  set iobuffer_2 [ create_bd_cell -type ip -vlnv user.org:user:iobuffer:1.0 iobuffer_2 ]

  # Create instance: iobuffer_4, and set properties
  set iobuffer_4 [ create_bd_cell -type ip -vlnv user.org:user:iobuffer:1.0 iobuffer_4 ]

  # Create instance: iobuffer_5, and set properties
  set iobuffer_5 [ create_bd_cell -type ip -vlnv user.org:user:iobuffer:1.0 iobuffer_5 ]

  # Create instance: iobuffer_6, and set properties
  set iobuffer_6 [ create_bd_cell -type ip -vlnv user.org:user:iobuffer:1.0 iobuffer_6 ]

  # Create instance: iobuffer_7, and set properties
  set iobuffer_7 [ create_bd_cell -type ip -vlnv user.org:user:iobuffer:1.0 iobuffer_7 ]

  # Create instance: pidiver_0, and set properties
  set pidiver_0 [ create_bd_cell -type ip -vlnv user.org:user:pidiver:1.0 pidiver_0 ]
  set_property -dict [ list \
   CONFIG.CALC_KECCAK384 {1} \
   CONFIG.CALC_TROIKA {1} \
   CONFIG.PARALLEL {6} \
 ] $pidiver_0

  # Create instance: proc_sys_reset_0, and set properties
  set proc_sys_reset_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 proc_sys_reset_0 ]

  # Create instance: proc_sys_reset_1, and set properties
  set proc_sys_reset_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 proc_sys_reset_1 ]

  # Create instance: util_vector_logic_0, and set properties
  set util_vector_logic_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_0 ]
  set_property -dict [ list \
   CONFIG.C_OPERATION {not} \
   CONFIG.LOGO_FILE {data/sym_notgate.png} \
 ] $util_vector_logic_0

  # Create instance: util_vector_logic_1, and set properties
  set util_vector_logic_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_1 ]
  set_property -dict [ list \
   CONFIG.C_OPERATION {not} \
 ] $util_vector_logic_1

  # Create instance: xlconcat_0, and set properties
  set xlconcat_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 xlconcat_0 ]
  set_property -dict [ list \
   CONFIG.IN0_WIDTH {1} \
   CONFIG.IN1_WIDTH {31} \
   CONFIG.NUM_PORTS {2} \
 ] $xlconcat_0

  # Create instance: xlconcat_1, and set properties
  set xlconcat_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 xlconcat_1 ]
  set_property -dict [ list \
   CONFIG.IN0_WIDTH {4} \
   CONFIG.IN1_WIDTH {1} \
   CONFIG.IN2_WIDTH {2} \
   CONFIG.IN3_WIDTH {1} \
   CONFIG.NUM_PORTS {4} \
 ] $xlconcat_1

  # Create instance: xlconstant_0, and set properties
  set xlconstant_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_0 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
 ] $xlconstant_0

  # Create instance: xlconstant_1, and set properties
  set xlconstant_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_1 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
 ] $xlconstant_1

  # Create instance: xlconstant_2, and set properties
  set xlconstant_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_2 ]
  set_property -dict [ list \
   CONFIG.CONST_WIDTH {2} \
 ] $xlconstant_2

  # Create instance: xlconstant_3, and set properties
  set xlconstant_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_3 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
   CONFIG.CONST_WIDTH {31} \
 ] $xlconstant_3

  # Create instance: xlconstant_4, and set properties
  set xlconstant_4 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_4 ]

  # Create instance: xlconstant_5, and set properties
  set xlconstant_5 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_5 ]
  set_property -dict [ list \
   CONFIG.CONST_WIDTH {1} \
 ] $xlconstant_5

  # Create instance: xlconstant_6, and set properties
  set xlconstant_6 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_6 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
   CONFIG.CONST_WIDTH {2} \
 ] $xlconstant_6

  # Create instance: xlslice_i2c, and set properties
  set xlslice_i2c [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_i2c ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {3} \
   CONFIG.DIN_TO {0} \
   CONFIG.DIN_WIDTH {8} \
   CONFIG.DOUT_WIDTH {4} \
 ] $xlslice_i2c

  # Create instance: xlslice_spi0_ss, and set properties
  set xlslice_spi0_ss [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_spi0_ss ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {5} \
   CONFIG.DIN_TO {5} \
   CONFIG.DIN_WIDTH {8} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_spi0_ss

  # Create instance: xlslice_w5500_rstn, and set properties
  set xlslice_w5500_rstn [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_w5500_rstn ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {4} \
   CONFIG.DIN_TO {4} \
   CONFIG.DIN_WIDTH {8} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_w5500_rstn

  # Create interface connections
  connect_bd_intf_net -intf_net CORTEXM1_AXI_0_CM1_AXI3 [get_bd_intf_pins CORTEXM1_AXI_0/CM1_AXI3] [get_bd_intf_pins axi_interconnect_0/S00_AXI]
  connect_bd_intf_net -intf_net axi_bram_ctrl_0_BRAM_PORTA [get_bd_intf_pins axi_bram_ctrl_0/BRAM_PORTA] [get_bd_intf_pins blk_mem_gen_0/BRAM_PORTA]
  connect_bd_intf_net -intf_net axi_gpio_1_GPIO [get_bd_intf_ports led_4bits] [get_bd_intf_pins axi_gpio_1/GPIO]
  connect_bd_intf_net -intf_net axi_gpio_1_GPIO2 [get_bd_intf_ports push_buttons_4bits] [get_bd_intf_pins axi_gpio_1/GPIO2]
  connect_bd_intf_net -intf_net axi_interconnect_0_M00_AXI [get_bd_intf_pins axi2lb_v1_0_0/s00_axi] [get_bd_intf_pins axi_interconnect_0/M00_AXI]
  connect_bd_intf_net -intf_net axi_interconnect_0_M01_AXI [get_bd_intf_pins axi2lb_v1_0_1/s00_axi] [get_bd_intf_pins axi_interconnect_0/M01_AXI]
  connect_bd_intf_net -intf_net axi_interconnect_0_M02_AXI [get_bd_intf_pins axi_bram_ctrl_0/S_AXI] [get_bd_intf_pins axi_interconnect_0/M02_AXI]
  connect_bd_intf_net -intf_net axi_interconnect_0_M03_AXI [get_bd_intf_pins axi_gpio_0/S_AXI] [get_bd_intf_pins axi_interconnect_0/M03_AXI]
  connect_bd_intf_net -intf_net axi_interconnect_0_M04_AXI [get_bd_intf_pins axi_interconnect_0/M04_AXI] [get_bd_intf_pins axi_uartlite_0/S_AXI]
  connect_bd_intf_net -intf_net axi_interconnect_0_M05_AXI [get_bd_intf_pins axi_interconnect_0/M05_AXI] [get_bd_intf_pins axi_quad_spi_0/AXI_LITE]
  connect_bd_intf_net -intf_net axi_interconnect_0_M06_AXI [get_bd_intf_pins axi_interconnect_0/M06_AXI] [get_bd_intf_pins axi_quad_spi_1/AXI_LITE]
  connect_bd_intf_net -intf_net axi_interconnect_0_M07_AXI [get_bd_intf_pins axi_gpio_1/S_AXI] [get_bd_intf_pins axi_interconnect_0/M07_AXI]
  connect_bd_intf_net -intf_net axi_uartlite_0_UART [get_bd_intf_ports usb_uart] [get_bd_intf_pins axi_uartlite_0/UART]

  # Create port connections
  connect_bd_net -net CORTEXM1_AXI_0_SWDO [get_bd_pins CORTEXM1_AXI_0/SWDO] [get_bd_pins SWD_Two_Wire_0/M1_SWDO]
  connect_bd_net -net CORTEXM1_AXI_0_SWDOEN [get_bd_pins CORTEXM1_AXI_0/SWDOEN] [get_bd_pins SWD_Two_Wire_0/M1_SWDOEN]
  connect_bd_net -net CORTEXM1_AXI_0_SYSRESETREQ [get_bd_pins CORTEXM1_AXI_0/SYSRESETREQ] [get_bd_pins proc_sys_reset_0/mb_debug_sys_rst]
  connect_bd_net -net Net [get_bd_ports spi_mosi0] [get_bd_pins iobuffer_0/io]
  connect_bd_net -net Net1 [get_bd_ports spi_miso0] [get_bd_pins iobuffer_1/io]
  connect_bd_net -net Net2 [get_bd_ports spi_sck0] [get_bd_pins iobuffer_2/io]
  connect_bd_net -net Net4 [get_bd_ports spi_mosi1] [get_bd_pins iobuffer_5/io]
  connect_bd_net -net Net5 [get_bd_ports spi_miso1] [get_bd_pins iobuffer_4/io]
  connect_bd_net -net Net6 [get_bd_ports spi_sck1] [get_bd_pins iobuffer_7/io]
  connect_bd_net -net Net7 [get_bd_ports spi_ss1] [get_bd_pins iobuffer_6/io]
  connect_bd_net -net SCL1 [get_bd_ports iic_scl_io] [get_bd_pins i2c_io_0/i2c_scl]
  connect_bd_net -net SDA1 [get_bd_ports iic_sda_io] [get_bd_pins i2c_io_0/i2c_sda]
  connect_bd_net -net SWCLK_1 [get_bd_ports SWCLK] [get_bd_pins SWD_Two_Wire_0/SWCLK]
  connect_bd_net -net SWDIO1 [get_bd_ports SWDIO] [get_bd_pins SWD_Two_Wire_0/SWDIO]
  connect_bd_net -net SWDRESET_1 [get_bd_ports SWDRESET] [get_bd_pins proc_sys_reset_0/aux_reset_in]
  connect_bd_net -net SWD_Two_Wire_0_M1_SWCLK [get_bd_pins CORTEXM1_AXI_0/SWCLKTCK] [get_bd_pins SWD_Two_Wire_0/M1_SWCLK]
  connect_bd_net -net SWD_Two_Wire_0_M1_SWDI [get_bd_pins CORTEXM1_AXI_0/SWDITMS] [get_bd_pins SWD_Two_Wire_0/M1_SWDI]
  connect_bd_net -net SWD_Two_Wire_0_lock_out [get_bd_pins SWD_Two_Wire_0/lock_out] [get_bd_pins xlconcat_1/In3]
  connect_bd_net -net axi2lb_v1_0_0_lread_addr [get_bd_pins axi2lb_v1_0_0/lread_addr] [get_bd_pins pidiver_0/read_address]
  connect_bd_net -net axi2lb_v1_0_0_lwrite_addr [get_bd_pins axi2lb_v1_0_0/lwrite_addr] [get_bd_pins pidiver_0/write_address]
  connect_bd_net -net axi2lb_v1_0_0_lwrite_data [get_bd_pins axi2lb_v1_0_0/lwrite_data] [get_bd_pins pidiver_0/write_data]
  connect_bd_net -net axi2lb_v1_0_0_lwrite_en [get_bd_pins axi2lb_v1_0_0/lwrite_en] [get_bd_pins pidiver_0/write_en]
  connect_bd_net -net axi2lb_v1_0_1_lread_addr [get_bd_pins axi2lb_v1_0_1/lread_addr] [get_bd_pins converter_0/read_address]
  connect_bd_net -net axi2lb_v1_0_1_lwrite_addr [get_bd_pins axi2lb_v1_0_1/lwrite_addr] [get_bd_pins converter_0/write_address]
  connect_bd_net -net axi2lb_v1_0_1_lwrite_data [get_bd_pins axi2lb_v1_0_1/lwrite_data] [get_bd_pins converter_0/write_data]
  connect_bd_net -net axi2lb_v1_0_1_lwrite_en [get_bd_pins axi2lb_v1_0_1/lwrite_en] [get_bd_pins converter_0/write_en]
  connect_bd_net -net axi_gpio_0_gpio_io_o [get_bd_pins axi_gpio_0/gpio_io_o] [get_bd_pins xlslice_i2c/Din] [get_bd_pins xlslice_spi0_ss/Din] [get_bd_pins xlslice_w5500_rstn/Din]
  connect_bd_net -net axi_quad_spi_0_io0_o [get_bd_pins axi_quad_spi_0/io0_o] [get_bd_pins iobuffer_0/i]
  connect_bd_net -net axi_quad_spi_0_io0_t [get_bd_pins axi_quad_spi_0/io0_t] [get_bd_pins iobuffer_0/t]
  connect_bd_net -net axi_quad_spi_0_io1_o [get_bd_pins axi_quad_spi_0/io1_o] [get_bd_pins iobuffer_1/i]
  connect_bd_net -net axi_quad_spi_0_io1_t [get_bd_pins axi_quad_spi_0/io1_t] [get_bd_pins iobuffer_1/t]
  connect_bd_net -net axi_quad_spi_0_sck_o [get_bd_pins axi_quad_spi_0/sck_o] [get_bd_pins iobuffer_2/i]
  connect_bd_net -net axi_quad_spi_0_sck_t [get_bd_pins axi_quad_spi_0/sck_t] [get_bd_pins iobuffer_2/t]
  connect_bd_net -net axi_quad_spi_1_io0_o [get_bd_pins axi_quad_spi_1/io0_o] [get_bd_pins iobuffer_5/i]
  connect_bd_net -net axi_quad_spi_1_io0_t [get_bd_pins axi_quad_spi_1/io0_t] [get_bd_pins iobuffer_5/t]
  connect_bd_net -net axi_quad_spi_1_io1_o [get_bd_pins axi_quad_spi_1/io1_o] [get_bd_pins iobuffer_4/i]
  connect_bd_net -net axi_quad_spi_1_io1_t [get_bd_pins axi_quad_spi_1/io1_t] [get_bd_pins iobuffer_4/t]
  connect_bd_net -net axi_quad_spi_1_sck_o [get_bd_pins axi_quad_spi_1/sck_o] [get_bd_pins iobuffer_7/i]
  connect_bd_net -net axi_quad_spi_1_sck_t [get_bd_pins axi_quad_spi_1/sck_t] [get_bd_pins iobuffer_7/t]
  connect_bd_net -net axi_quad_spi_1_ss_o [get_bd_pins axi_quad_spi_1/ss_o] [get_bd_pins iobuffer_6/i]
  connect_bd_net -net axi_quad_spi_1_ss_t [get_bd_pins axi_quad_spi_1/ss_t] [get_bd_pins iobuffer_6/t]
  connect_bd_net -net axi_uartlite_0_interrupt [get_bd_pins axi_uartlite_0/interrupt] [get_bd_pins xlconcat_0/In0]
  connect_bd_net -net blk_mem_gen_0_doutb [get_bd_pins blk_mem_gen_0/doutb] [get_bd_pins converter_0/ram_data_out]
  connect_bd_net -net clk_wiz_0_clk_out1 [get_bd_pins CORTEXM1_AXI_0/HCLK] [get_bd_pins axi2lb_v1_0_0/s00_axi_aclk] [get_bd_pins axi2lb_v1_0_1/s00_axi_aclk] [get_bd_pins axi_bram_ctrl_0/s_axi_aclk] [get_bd_pins axi_gpio_0/s_axi_aclk] [get_bd_pins axi_gpio_1/s_axi_aclk] [get_bd_pins axi_interconnect_0/ACLK] [get_bd_pins axi_interconnect_0/M00_ACLK] [get_bd_pins axi_interconnect_0/M01_ACLK] [get_bd_pins axi_interconnect_0/M02_ACLK] [get_bd_pins axi_interconnect_0/M03_ACLK] [get_bd_pins axi_interconnect_0/M04_ACLK] [get_bd_pins axi_interconnect_0/M05_ACLK] [get_bd_pins axi_interconnect_0/M06_ACLK] [get_bd_pins axi_interconnect_0/M07_ACLK] [get_bd_pins axi_interconnect_0/M08_ACLK] [get_bd_pins axi_interconnect_0/S00_ACLK] [get_bd_pins axi_quad_spi_0/ext_spi_clk] [get_bd_pins axi_quad_spi_0/s_axi_aclk] [get_bd_pins axi_quad_spi_1/ext_spi_clk] [get_bd_pins axi_quad_spi_1/s_axi_aclk] [get_bd_pins axi_uartlite_0/s_axi_aclk] [get_bd_pins blk_mem_gen_0/clkb] [get_bd_pins clk_wiz_0/clk_out1] [get_bd_pins converter_0/clk] [get_bd_pins pidiver_0/clk_slow] [get_bd_pins proc_sys_reset_0/slowest_sync_clk] [get_bd_pins proc_sys_reset_1/slowest_sync_clk]
  connect_bd_net -net clk_wiz_0_clk_out3 [get_bd_pins clk_wiz_0/clk_out3] [get_bd_pins pidiver_0/clk_fast]
  connect_bd_net -net clk_wiz_0_locked [get_bd_pins clk_wiz_0/locked] [get_bd_pins proc_sys_reset_0/dcm_locked]
  connect_bd_net -net converter_0_ram_addr [get_bd_pins blk_mem_gen_0/addrb] [get_bd_pins converter_0/ram_addr]
  connect_bd_net -net converter_0_ram_data_in [get_bd_pins blk_mem_gen_0/dinb] [get_bd_pins converter_0/ram_data_in]
  connect_bd_net -net converter_0_ram_en [get_bd_pins blk_mem_gen_0/enb] [get_bd_pins converter_0/ram_en]
  connect_bd_net -net converter_0_ram_we [get_bd_pins blk_mem_gen_0/web] [get_bd_pins converter_0/ram_we]
  connect_bd_net -net converter_0_read_data [get_bd_pins axi2lb_v1_0_1/lread_data] [get_bd_pins converter_0/read_data]
  connect_bd_net -net i2c_io_0_gpio_out [get_bd_pins i2c_io_0/gpio_out] [get_bd_pins xlconcat_1/In0]
  connect_bd_net -net iobuffer_0_o [get_bd_pins axi_quad_spi_0/io0_i] [get_bd_pins iobuffer_0/o]
  connect_bd_net -net iobuffer_1_o [get_bd_pins axi_quad_spi_0/io1_i] [get_bd_pins iobuffer_1/o]
  connect_bd_net -net iobuffer_2_o [get_bd_pins axi_quad_spi_0/sck_i] [get_bd_pins iobuffer_2/o]
  connect_bd_net -net iobuffer_4_o [get_bd_pins axi_quad_spi_1/io1_i] [get_bd_pins iobuffer_4/o]
  connect_bd_net -net iobuffer_5_o [get_bd_pins axi_quad_spi_1/io0_i] [get_bd_pins iobuffer_5/o]
  connect_bd_net -net iobuffer_6_o [get_bd_pins axi_quad_spi_1/ss_i] [get_bd_pins iobuffer_6/o]
  connect_bd_net -net iobuffer_7_o [get_bd_pins axi_quad_spi_1/sck_i] [get_bd_pins iobuffer_7/o]
  connect_bd_net -net pidiver_0_read_data [get_bd_pins axi2lb_v1_0_0/lread_data] [get_bd_pins pidiver_0/read_data]
  connect_bd_net -net proc_sys_reset_0_interconnect_aresetn [get_bd_pins axi_interconnect_0/ARESETN] [get_bd_pins axi_interconnect_0/M00_ARESETN] [get_bd_pins axi_interconnect_0/M01_ARESETN] [get_bd_pins axi_interconnect_0/M02_ARESETN] [get_bd_pins axi_interconnect_0/M03_ARESETN] [get_bd_pins axi_interconnect_0/M04_ARESETN] [get_bd_pins axi_interconnect_0/M05_ARESETN] [get_bd_pins axi_interconnect_0/M06_ARESETN] [get_bd_pins axi_interconnect_0/M07_ARESETN] [get_bd_pins axi_interconnect_0/M08_ARESETN] [get_bd_pins axi_interconnect_0/S00_ARESETN] [get_bd_pins proc_sys_reset_0/interconnect_aresetn]
  connect_bd_net -net proc_sys_reset_0_mb_reset [get_bd_pins proc_sys_reset_0/mb_reset] [get_bd_pins util_vector_logic_0/Op1]
  connect_bd_net -net proc_sys_reset_0_peripheral_aresetn [get_bd_pins axi2lb_v1_0_0/s00_axi_aresetn] [get_bd_pins axi2lb_v1_0_1/s00_axi_aresetn] [get_bd_pins axi_bram_ctrl_0/s_axi_aresetn] [get_bd_pins axi_gpio_0/s_axi_aresetn] [get_bd_pins axi_gpio_1/s_axi_aresetn] [get_bd_pins axi_quad_spi_0/s_axi_aresetn] [get_bd_pins axi_quad_spi_1/s_axi_aresetn] [get_bd_pins axi_uartlite_0/s_axi_aresetn] [get_bd_pins converter_0/reset] [get_bd_pins pidiver_0/reset_slow] [get_bd_pins proc_sys_reset_0/peripheral_aresetn]
  connect_bd_net -net proc_sys_reset_1_mb_reset [get_bd_pins proc_sys_reset_1/mb_reset] [get_bd_pins util_vector_logic_1/Op1]
  connect_bd_net -net reset_1 [get_bd_ports reset] [get_bd_pins proc_sys_reset_0/ext_reset_in]
  connect_bd_net -net sys_clock_1 [get_bd_ports sys_clock] [get_bd_pins clk_wiz_0/clk_in1]
  connect_bd_net -net util_vector_logic_0_Res [get_bd_pins CORTEXM1_AXI_0/SYSRESETn] [get_bd_pins util_vector_logic_0/Res]
  connect_bd_net -net util_vector_logic_1_Res [get_bd_pins CORTEXM1_AXI_0/DBGRESETn] [get_bd_pins util_vector_logic_1/Res]
  connect_bd_net -net w5500_intn_1 [get_bd_ports w5500_intn] [get_bd_pins xlconcat_1/In1]
  connect_bd_net -net xlconcat_0_dout [get_bd_pins CORTEXM1_AXI_0/IRQ] [get_bd_pins xlconcat_0/dout]
  connect_bd_net -net xlconcat_1_dout [get_bd_pins axi_gpio_0/gpio2_io_i] [get_bd_pins xlconcat_1/dout]
  connect_bd_net -net xlconstant_0_dout [get_bd_pins blk_mem_gen_0/rstb] [get_bd_pins xlconstant_0/dout]
  connect_bd_net -net xlconstant_1_dout [get_bd_ports TDO] [get_bd_pins xlconstant_1/dout]
  connect_bd_net -net xlconstant_2_dout [get_bd_pins CORTEXM1_AXI_0/CFGITCMEN] [get_bd_pins xlconstant_2/dout]
  connect_bd_net -net xlconstant_3_dout [get_bd_pins xlconcat_0/In1] [get_bd_pins xlconstant_3/dout]
  connect_bd_net -net xlconstant_4_dout [get_bd_pins pidiver_0/reset_fast] [get_bd_pins xlconstant_4/dout]
  connect_bd_net -net xlconstant_5_dout [get_bd_pins proc_sys_reset_1/aux_reset_in] [get_bd_pins proc_sys_reset_1/ext_reset_in] [get_bd_pins xlconstant_5/dout]
  connect_bd_net -net xlconstant_6_dout [get_bd_pins xlconcat_1/In2] [get_bd_pins xlconstant_6/dout]
  connect_bd_net -net xlslice_0_Dout [get_bd_ports w5500_rstn] [get_bd_pins xlslice_w5500_rstn/Dout]
  connect_bd_net -net xlslice_1_Dout [get_bd_pins i2c_io_0/gpio_in] [get_bd_pins xlslice_i2c/Dout]
  connect_bd_net -net xlslice_2_Dout [get_bd_ports spi_ss0] [get_bd_pins xlslice_spi0_ss/Dout]

  # Create address segments
  create_bd_addr_seg -range 0x00010000 -offset 0x44A00000 [get_bd_addr_spaces CORTEXM1_AXI_0/CM1_AXI3] [get_bd_addr_segs axi2lb_v1_0_0/s00_axi/reg0] SEG_axi2lb_v1_0_0_reg0
  create_bd_addr_seg -range 0x00010000 -offset 0x44A20000 [get_bd_addr_spaces CORTEXM1_AXI_0/CM1_AXI3] [get_bd_addr_segs axi2lb_v1_0_1/s00_axi/reg0] SEG_axi2lb_v1_0_1_reg0
  create_bd_addr_seg -range 0x00002000 -offset 0xC0000000 [get_bd_addr_spaces CORTEXM1_AXI_0/CM1_AXI3] [get_bd_addr_segs axi_bram_ctrl_0/S_AXI/Mem0] SEG_axi_bram_ctrl_0_Mem0
  create_bd_addr_seg -range 0x00010000 -offset 0x40000000 [get_bd_addr_spaces CORTEXM1_AXI_0/CM1_AXI3] [get_bd_addr_segs axi_gpio_0/S_AXI/Reg] SEG_axi_gpio_0_Reg
  create_bd_addr_seg -range 0x00010000 -offset 0x40010000 [get_bd_addr_spaces CORTEXM1_AXI_0/CM1_AXI3] [get_bd_addr_segs axi_gpio_1/S_AXI/Reg] SEG_axi_gpio_1_Reg
  create_bd_addr_seg -range 0x00010000 -offset 0x44A10000 [get_bd_addr_spaces CORTEXM1_AXI_0/CM1_AXI3] [get_bd_addr_segs axi_quad_spi_0/AXI_LITE/Reg] SEG_axi_quad_spi_0_Reg
  create_bd_addr_seg -range 0x00010000 -offset 0x44A30000 [get_bd_addr_spaces CORTEXM1_AXI_0/CM1_AXI3] [get_bd_addr_segs axi_quad_spi_1/AXI_LITE/Reg] SEG_axi_quad_spi_1_Reg
  create_bd_addr_seg -range 0x00010000 -offset 0x40600000 [get_bd_addr_spaces CORTEXM1_AXI_0/CM1_AXI3] [get_bd_addr_segs axi_uartlite_0/S_AXI/Reg] SEG_axi_uartlite_0_Reg


  # Restore current instance
  current_bd_instance $oldCurInst

  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""



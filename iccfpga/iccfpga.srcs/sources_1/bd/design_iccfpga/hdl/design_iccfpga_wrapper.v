//Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
//Date        : Fri Mar 15 22:13:11 2019
//Host        : kubuntu running 64-bit Ubuntu 18.04.1 LTS
//Command     : generate_target design_iccfpga_wrapper.bd
//Design      : design_iccfpga_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_iccfpga_wrapper
   (SWCLK,
    SWDIO,
    SWDRESET,
    TDI,
    TDO,
    iic_scl_io,
    iic_sda_io,
    led_4bits_tri_io,
    nTRST,
    push_buttons_4bits_tri_i,
    reset,
    spi_miso0,
    spi_miso1,
    spi_mosi0,
    spi_mosi1,
    spi_sck0,
    spi_sck1,
    spi_ss0,
    spi_ss1,
    sys_clock,
    usb_uart_rxd,
    usb_uart_txd,
    w5500_intn,
    w5500_rstn);
  input SWCLK;
  inout SWDIO;
  input SWDRESET;
  input TDI;
  output [0:0]TDO;
  inout iic_scl_io;
  inout iic_sda_io;
  inout [3:0]led_4bits_tri_io;
  input nTRST;
  input [3:0]push_buttons_4bits_tri_i;
  input reset;
  inout spi_miso0;
  inout spi_miso1;
  inout spi_mosi0;
  inout spi_mosi1;
  inout spi_sck0;
  inout spi_sck1;
  output [0:0]spi_ss0;
  inout spi_ss1;
  input sys_clock;
  input usb_uart_rxd;
  output usb_uart_txd;
  input w5500_intn;
  output [0:0]w5500_rstn;

  wire SWCLK;
  wire SWDIO;
  wire SWDRESET;
  wire TDI;
  wire [0:0]TDO;
  wire iic_scl_io;
  wire iic_sda_io;
  wire [0:0]led_4bits_tri_i_0;
  wire [1:1]led_4bits_tri_i_1;
  wire [2:2]led_4bits_tri_i_2;
  wire [3:3]led_4bits_tri_i_3;
  wire [0:0]led_4bits_tri_io_0;
  wire [1:1]led_4bits_tri_io_1;
  wire [2:2]led_4bits_tri_io_2;
  wire [3:3]led_4bits_tri_io_3;
  wire [0:0]led_4bits_tri_o_0;
  wire [1:1]led_4bits_tri_o_1;
  wire [2:2]led_4bits_tri_o_2;
  wire [3:3]led_4bits_tri_o_3;
  wire [0:0]led_4bits_tri_t_0;
  wire [1:1]led_4bits_tri_t_1;
  wire [2:2]led_4bits_tri_t_2;
  wire [3:3]led_4bits_tri_t_3;
  wire nTRST;
  wire [3:0]push_buttons_4bits_tri_i;
  wire reset;
  wire spi_miso0;
  wire spi_miso1;
  wire spi_mosi0;
  wire spi_mosi1;
  wire spi_sck0;
  wire spi_sck1;
  wire [0:0]spi_ss0;
  wire spi_ss1;
  wire sys_clock;
  wire usb_uart_rxd;
  wire usb_uart_txd;
  wire w5500_intn;
  wire [0:0]w5500_rstn;

  design_iccfpga design_iccfpga_i
       (.SWCLK(SWCLK),
        .SWDIO(SWDIO),
        .SWDRESET(SWDRESET),
        .TDI(TDI),
        .TDO(TDO),
        .iic_scl_io(iic_scl_io),
        .iic_sda_io(iic_sda_io),
        .led_4bits_tri_i({led_4bits_tri_i_3,led_4bits_tri_i_2,led_4bits_tri_i_1,led_4bits_tri_i_0}),
        .led_4bits_tri_o({led_4bits_tri_o_3,led_4bits_tri_o_2,led_4bits_tri_o_1,led_4bits_tri_o_0}),
        .led_4bits_tri_t({led_4bits_tri_t_3,led_4bits_tri_t_2,led_4bits_tri_t_1,led_4bits_tri_t_0}),
        .nTRST(nTRST),
        .push_buttons_4bits_tri_i(push_buttons_4bits_tri_i),
        .reset(reset),
        .spi_miso0(spi_miso0),
        .spi_miso1(spi_miso1),
        .spi_mosi0(spi_mosi0),
        .spi_mosi1(spi_mosi1),
        .spi_sck0(spi_sck0),
        .spi_sck1(spi_sck1),
        .spi_ss0(spi_ss0),
        .spi_ss1(spi_ss1),
        .sys_clock(sys_clock),
        .usb_uart_rxd(usb_uart_rxd),
        .usb_uart_txd(usb_uart_txd),
        .w5500_intn(w5500_intn),
        .w5500_rstn(w5500_rstn));
  IOBUF led_4bits_tri_iobuf_0
       (.I(led_4bits_tri_o_0),
        .IO(led_4bits_tri_io[0]),
        .O(led_4bits_tri_i_0),
        .T(led_4bits_tri_t_0));
  IOBUF led_4bits_tri_iobuf_1
       (.I(led_4bits_tri_o_1),
        .IO(led_4bits_tri_io[1]),
        .O(led_4bits_tri_i_1),
        .T(led_4bits_tri_t_1));
  IOBUF led_4bits_tri_iobuf_2
       (.I(led_4bits_tri_o_2),
        .IO(led_4bits_tri_io[2]),
        .O(led_4bits_tri_i_2),
        .T(led_4bits_tri_t_2));
  IOBUF led_4bits_tri_iobuf_3
       (.I(led_4bits_tri_o_3),
        .IO(led_4bits_tri_io[3]),
        .O(led_4bits_tri_i_3),
        .T(led_4bits_tri_t_3));
endmodule

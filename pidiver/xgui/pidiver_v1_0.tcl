# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "BITS_MIN_WEIGHT_MAGINUTE_MAX" -parent ${Page_0}
  ipgui::add_param $IPINST -name "CALC_KECCAK384" -parent ${Page_0}
  ipgui::add_param $IPINST -name "CALC_POW" -parent ${Page_0}
  ipgui::add_param $IPINST -name "CALC_TROIKA" -parent ${Page_0}
  ipgui::add_param $IPINST -name "CLOCK_SYNC" -parent ${Page_0}
  ipgui::add_param $IPINST -name "COLUMNS" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "ENABLE_STOP" -parent ${Page_0}
  ipgui::add_param $IPINST -name "HASH_LENGTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "INTERN_NONCE_LENGTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "NONCE_LENGTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "NONCE_OFFSET" -parent ${Page_0}
  ipgui::add_param $IPINST -name "NUMBER_OF_ROUNDS" -parent ${Page_0}
  ipgui::add_param $IPINST -name "NUM_SBOXES" -parent ${Page_0}
  ipgui::add_param $IPINST -name "PARALLEL" -parent ${Page_0}
  ipgui::add_param $IPINST -name "ROWS" -parent ${Page_0}
  ipgui::add_param $IPINST -name "SHA3_384_BLOCK_LENGTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "SHA3_384_BLOCK_LENGTH_BITS" -parent ${Page_0}
  ipgui::add_param $IPINST -name "SHA3_384_DIGEST_LENGTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "SHA3_384_HASH_SIZE" -parent ${Page_0}
  ipgui::add_param $IPINST -name "SHA3_MAX_PERMUTATION_SIZE" -parent ${Page_0}
  ipgui::add_param $IPINST -name "SHA3_MAX_RATE_IN_QWORDS" -parent ${Page_0}
  ipgui::add_param $IPINST -name "SHA3_NUM_ROUNDS" -parent ${Page_0}
  ipgui::add_param $IPINST -name "SLICES" -parent ${Page_0}
  ipgui::add_param $IPINST -name "SLICESIZE" -parent ${Page_0}
  ipgui::add_param $IPINST -name "STATESIZE" -parent ${Page_0}
  ipgui::add_param $IPINST -name "STATE_LENGTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "VERSION_MAJOR" -parent ${Page_0}
  ipgui::add_param $IPINST -name "VERSION_MINOR" -parent ${Page_0}


}

proc update_PARAM_VALUE.BITS_MIN_WEIGHT_MAGINUTE_MAX { PARAM_VALUE.BITS_MIN_WEIGHT_MAGINUTE_MAX } {
	# Procedure called to update BITS_MIN_WEIGHT_MAGINUTE_MAX when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.BITS_MIN_WEIGHT_MAGINUTE_MAX { PARAM_VALUE.BITS_MIN_WEIGHT_MAGINUTE_MAX } {
	# Procedure called to validate BITS_MIN_WEIGHT_MAGINUTE_MAX
	return true
}

proc update_PARAM_VALUE.CALC_KECCAK384 { PARAM_VALUE.CALC_KECCAK384 } {
	# Procedure called to update CALC_KECCAK384 when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CALC_KECCAK384 { PARAM_VALUE.CALC_KECCAK384 } {
	# Procedure called to validate CALC_KECCAK384
	return true
}

proc update_PARAM_VALUE.CALC_POW { PARAM_VALUE.CALC_POW } {
	# Procedure called to update CALC_POW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CALC_POW { PARAM_VALUE.CALC_POW } {
	# Procedure called to validate CALC_POW
	return true
}

proc update_PARAM_VALUE.CALC_TROIKA { PARAM_VALUE.CALC_TROIKA } {
	# Procedure called to update CALC_TROIKA when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CALC_TROIKA { PARAM_VALUE.CALC_TROIKA } {
	# Procedure called to validate CALC_TROIKA
	return true
}

proc update_PARAM_VALUE.CLOCK_SYNC { PARAM_VALUE.CLOCK_SYNC } {
	# Procedure called to update CLOCK_SYNC when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CLOCK_SYNC { PARAM_VALUE.CLOCK_SYNC } {
	# Procedure called to validate CLOCK_SYNC
	return true
}

proc update_PARAM_VALUE.COLUMNS { PARAM_VALUE.COLUMNS } {
	# Procedure called to update COLUMNS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.COLUMNS { PARAM_VALUE.COLUMNS } {
	# Procedure called to validate COLUMNS
	return true
}

proc update_PARAM_VALUE.DATA_WIDTH { PARAM_VALUE.DATA_WIDTH } {
	# Procedure called to update DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DATA_WIDTH { PARAM_VALUE.DATA_WIDTH } {
	# Procedure called to validate DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.ENABLE_STOP { PARAM_VALUE.ENABLE_STOP } {
	# Procedure called to update ENABLE_STOP when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.ENABLE_STOP { PARAM_VALUE.ENABLE_STOP } {
	# Procedure called to validate ENABLE_STOP
	return true
}

proc update_PARAM_VALUE.HASH_LENGTH { PARAM_VALUE.HASH_LENGTH } {
	# Procedure called to update HASH_LENGTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.HASH_LENGTH { PARAM_VALUE.HASH_LENGTH } {
	# Procedure called to validate HASH_LENGTH
	return true
}

proc update_PARAM_VALUE.INTERN_NONCE_LENGTH { PARAM_VALUE.INTERN_NONCE_LENGTH } {
	# Procedure called to update INTERN_NONCE_LENGTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.INTERN_NONCE_LENGTH { PARAM_VALUE.INTERN_NONCE_LENGTH } {
	# Procedure called to validate INTERN_NONCE_LENGTH
	return true
}

proc update_PARAM_VALUE.NONCE_LENGTH { PARAM_VALUE.NONCE_LENGTH } {
	# Procedure called to update NONCE_LENGTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.NONCE_LENGTH { PARAM_VALUE.NONCE_LENGTH } {
	# Procedure called to validate NONCE_LENGTH
	return true
}

proc update_PARAM_VALUE.NONCE_OFFSET { PARAM_VALUE.NONCE_OFFSET } {
	# Procedure called to update NONCE_OFFSET when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.NONCE_OFFSET { PARAM_VALUE.NONCE_OFFSET } {
	# Procedure called to validate NONCE_OFFSET
	return true
}

proc update_PARAM_VALUE.NUMBER_OF_ROUNDS { PARAM_VALUE.NUMBER_OF_ROUNDS } {
	# Procedure called to update NUMBER_OF_ROUNDS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.NUMBER_OF_ROUNDS { PARAM_VALUE.NUMBER_OF_ROUNDS } {
	# Procedure called to validate NUMBER_OF_ROUNDS
	return true
}

proc update_PARAM_VALUE.NUM_SBOXES { PARAM_VALUE.NUM_SBOXES } {
	# Procedure called to update NUM_SBOXES when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.NUM_SBOXES { PARAM_VALUE.NUM_SBOXES } {
	# Procedure called to validate NUM_SBOXES
	return true
}

proc update_PARAM_VALUE.PARALLEL { PARAM_VALUE.PARALLEL } {
	# Procedure called to update PARALLEL when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.PARALLEL { PARAM_VALUE.PARALLEL } {
	# Procedure called to validate PARALLEL
	return true
}

proc update_PARAM_VALUE.ROWS { PARAM_VALUE.ROWS } {
	# Procedure called to update ROWS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.ROWS { PARAM_VALUE.ROWS } {
	# Procedure called to validate ROWS
	return true
}

proc update_PARAM_VALUE.SHA3_384_BLOCK_LENGTH { PARAM_VALUE.SHA3_384_BLOCK_LENGTH } {
	# Procedure called to update SHA3_384_BLOCK_LENGTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SHA3_384_BLOCK_LENGTH { PARAM_VALUE.SHA3_384_BLOCK_LENGTH } {
	# Procedure called to validate SHA3_384_BLOCK_LENGTH
	return true
}

proc update_PARAM_VALUE.SHA3_384_BLOCK_LENGTH_BITS { PARAM_VALUE.SHA3_384_BLOCK_LENGTH_BITS } {
	# Procedure called to update SHA3_384_BLOCK_LENGTH_BITS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SHA3_384_BLOCK_LENGTH_BITS { PARAM_VALUE.SHA3_384_BLOCK_LENGTH_BITS } {
	# Procedure called to validate SHA3_384_BLOCK_LENGTH_BITS
	return true
}

proc update_PARAM_VALUE.SHA3_384_DIGEST_LENGTH { PARAM_VALUE.SHA3_384_DIGEST_LENGTH } {
	# Procedure called to update SHA3_384_DIGEST_LENGTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SHA3_384_DIGEST_LENGTH { PARAM_VALUE.SHA3_384_DIGEST_LENGTH } {
	# Procedure called to validate SHA3_384_DIGEST_LENGTH
	return true
}

proc update_PARAM_VALUE.SHA3_384_HASH_SIZE { PARAM_VALUE.SHA3_384_HASH_SIZE } {
	# Procedure called to update SHA3_384_HASH_SIZE when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SHA3_384_HASH_SIZE { PARAM_VALUE.SHA3_384_HASH_SIZE } {
	# Procedure called to validate SHA3_384_HASH_SIZE
	return true
}

proc update_PARAM_VALUE.SHA3_MAX_PERMUTATION_SIZE { PARAM_VALUE.SHA3_MAX_PERMUTATION_SIZE } {
	# Procedure called to update SHA3_MAX_PERMUTATION_SIZE when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SHA3_MAX_PERMUTATION_SIZE { PARAM_VALUE.SHA3_MAX_PERMUTATION_SIZE } {
	# Procedure called to validate SHA3_MAX_PERMUTATION_SIZE
	return true
}

proc update_PARAM_VALUE.SHA3_MAX_RATE_IN_QWORDS { PARAM_VALUE.SHA3_MAX_RATE_IN_QWORDS } {
	# Procedure called to update SHA3_MAX_RATE_IN_QWORDS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SHA3_MAX_RATE_IN_QWORDS { PARAM_VALUE.SHA3_MAX_RATE_IN_QWORDS } {
	# Procedure called to validate SHA3_MAX_RATE_IN_QWORDS
	return true
}

proc update_PARAM_VALUE.SHA3_NUM_ROUNDS { PARAM_VALUE.SHA3_NUM_ROUNDS } {
	# Procedure called to update SHA3_NUM_ROUNDS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SHA3_NUM_ROUNDS { PARAM_VALUE.SHA3_NUM_ROUNDS } {
	# Procedure called to validate SHA3_NUM_ROUNDS
	return true
}

proc update_PARAM_VALUE.SLICES { PARAM_VALUE.SLICES } {
	# Procedure called to update SLICES when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SLICES { PARAM_VALUE.SLICES } {
	# Procedure called to validate SLICES
	return true
}

proc update_PARAM_VALUE.SLICESIZE { PARAM_VALUE.SLICESIZE } {
	# Procedure called to update SLICESIZE when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SLICESIZE { PARAM_VALUE.SLICESIZE } {
	# Procedure called to validate SLICESIZE
	return true
}

proc update_PARAM_VALUE.STATESIZE { PARAM_VALUE.STATESIZE } {
	# Procedure called to update STATESIZE when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.STATESIZE { PARAM_VALUE.STATESIZE } {
	# Procedure called to validate STATESIZE
	return true
}

proc update_PARAM_VALUE.STATE_LENGTH { PARAM_VALUE.STATE_LENGTH } {
	# Procedure called to update STATE_LENGTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.STATE_LENGTH { PARAM_VALUE.STATE_LENGTH } {
	# Procedure called to validate STATE_LENGTH
	return true
}

proc update_PARAM_VALUE.VERSION_MAJOR { PARAM_VALUE.VERSION_MAJOR } {
	# Procedure called to update VERSION_MAJOR when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.VERSION_MAJOR { PARAM_VALUE.VERSION_MAJOR } {
	# Procedure called to validate VERSION_MAJOR
	return true
}

proc update_PARAM_VALUE.VERSION_MINOR { PARAM_VALUE.VERSION_MINOR } {
	# Procedure called to update VERSION_MINOR when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.VERSION_MINOR { PARAM_VALUE.VERSION_MINOR } {
	# Procedure called to validate VERSION_MINOR
	return true
}


proc update_MODELPARAM_VALUE.VERSION_MAJOR { MODELPARAM_VALUE.VERSION_MAJOR PARAM_VALUE.VERSION_MAJOR } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.VERSION_MAJOR}] ${MODELPARAM_VALUE.VERSION_MAJOR}
}

proc update_MODELPARAM_VALUE.VERSION_MINOR { MODELPARAM_VALUE.VERSION_MINOR PARAM_VALUE.VERSION_MINOR } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.VERSION_MINOR}] ${MODELPARAM_VALUE.VERSION_MINOR}
}

proc update_MODELPARAM_VALUE.CALC_POW { MODELPARAM_VALUE.CALC_POW PARAM_VALUE.CALC_POW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CALC_POW}] ${MODELPARAM_VALUE.CALC_POW}
}

proc update_MODELPARAM_VALUE.HASH_LENGTH { MODELPARAM_VALUE.HASH_LENGTH PARAM_VALUE.HASH_LENGTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.HASH_LENGTH}] ${MODELPARAM_VALUE.HASH_LENGTH}
}

proc update_MODELPARAM_VALUE.STATE_LENGTH { MODELPARAM_VALUE.STATE_LENGTH PARAM_VALUE.STATE_LENGTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.STATE_LENGTH}] ${MODELPARAM_VALUE.STATE_LENGTH}
}

proc update_MODELPARAM_VALUE.NONCE_LENGTH { MODELPARAM_VALUE.NONCE_LENGTH PARAM_VALUE.NONCE_LENGTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.NONCE_LENGTH}] ${MODELPARAM_VALUE.NONCE_LENGTH}
}

proc update_MODELPARAM_VALUE.NUMBER_OF_ROUNDS { MODELPARAM_VALUE.NUMBER_OF_ROUNDS PARAM_VALUE.NUMBER_OF_ROUNDS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.NUMBER_OF_ROUNDS}] ${MODELPARAM_VALUE.NUMBER_OF_ROUNDS}
}

proc update_MODELPARAM_VALUE.PARALLEL { MODELPARAM_VALUE.PARALLEL PARAM_VALUE.PARALLEL } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.PARALLEL}] ${MODELPARAM_VALUE.PARALLEL}
}

proc update_MODELPARAM_VALUE.INTERN_NONCE_LENGTH { MODELPARAM_VALUE.INTERN_NONCE_LENGTH PARAM_VALUE.INTERN_NONCE_LENGTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.INTERN_NONCE_LENGTH}] ${MODELPARAM_VALUE.INTERN_NONCE_LENGTH}
}

proc update_MODELPARAM_VALUE.BITS_MIN_WEIGHT_MAGINUTE_MAX { MODELPARAM_VALUE.BITS_MIN_WEIGHT_MAGINUTE_MAX PARAM_VALUE.BITS_MIN_WEIGHT_MAGINUTE_MAX } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.BITS_MIN_WEIGHT_MAGINUTE_MAX}] ${MODELPARAM_VALUE.BITS_MIN_WEIGHT_MAGINUTE_MAX}
}

proc update_MODELPARAM_VALUE.DATA_WIDTH { MODELPARAM_VALUE.DATA_WIDTH PARAM_VALUE.DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DATA_WIDTH}] ${MODELPARAM_VALUE.DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.NONCE_OFFSET { MODELPARAM_VALUE.NONCE_OFFSET PARAM_VALUE.NONCE_OFFSET } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.NONCE_OFFSET}] ${MODELPARAM_VALUE.NONCE_OFFSET}
}

proc update_MODELPARAM_VALUE.CLOCK_SYNC { MODELPARAM_VALUE.CLOCK_SYNC PARAM_VALUE.CLOCK_SYNC } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CLOCK_SYNC}] ${MODELPARAM_VALUE.CLOCK_SYNC}
}

proc update_MODELPARAM_VALUE.ENABLE_STOP { MODELPARAM_VALUE.ENABLE_STOP PARAM_VALUE.ENABLE_STOP } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.ENABLE_STOP}] ${MODELPARAM_VALUE.ENABLE_STOP}
}

proc update_MODELPARAM_VALUE.CALC_KECCAK384 { MODELPARAM_VALUE.CALC_KECCAK384 PARAM_VALUE.CALC_KECCAK384 } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CALC_KECCAK384}] ${MODELPARAM_VALUE.CALC_KECCAK384}
}

proc update_MODELPARAM_VALUE.SHA3_384_HASH_SIZE { MODELPARAM_VALUE.SHA3_384_HASH_SIZE PARAM_VALUE.SHA3_384_HASH_SIZE } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SHA3_384_HASH_SIZE}] ${MODELPARAM_VALUE.SHA3_384_HASH_SIZE}
}

proc update_MODELPARAM_VALUE.SHA3_384_DIGEST_LENGTH { MODELPARAM_VALUE.SHA3_384_DIGEST_LENGTH PARAM_VALUE.SHA3_384_DIGEST_LENGTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SHA3_384_DIGEST_LENGTH}] ${MODELPARAM_VALUE.SHA3_384_DIGEST_LENGTH}
}

proc update_MODELPARAM_VALUE.SHA3_MAX_PERMUTATION_SIZE { MODELPARAM_VALUE.SHA3_MAX_PERMUTATION_SIZE PARAM_VALUE.SHA3_MAX_PERMUTATION_SIZE } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SHA3_MAX_PERMUTATION_SIZE}] ${MODELPARAM_VALUE.SHA3_MAX_PERMUTATION_SIZE}
}

proc update_MODELPARAM_VALUE.SHA3_MAX_RATE_IN_QWORDS { MODELPARAM_VALUE.SHA3_MAX_RATE_IN_QWORDS PARAM_VALUE.SHA3_MAX_RATE_IN_QWORDS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SHA3_MAX_RATE_IN_QWORDS}] ${MODELPARAM_VALUE.SHA3_MAX_RATE_IN_QWORDS}
}

proc update_MODELPARAM_VALUE.SHA3_384_BLOCK_LENGTH { MODELPARAM_VALUE.SHA3_384_BLOCK_LENGTH PARAM_VALUE.SHA3_384_BLOCK_LENGTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SHA3_384_BLOCK_LENGTH}] ${MODELPARAM_VALUE.SHA3_384_BLOCK_LENGTH}
}

proc update_MODELPARAM_VALUE.SHA3_384_BLOCK_LENGTH_BITS { MODELPARAM_VALUE.SHA3_384_BLOCK_LENGTH_BITS PARAM_VALUE.SHA3_384_BLOCK_LENGTH_BITS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SHA3_384_BLOCK_LENGTH_BITS}] ${MODELPARAM_VALUE.SHA3_384_BLOCK_LENGTH_BITS}
}

proc update_MODELPARAM_VALUE.SHA3_NUM_ROUNDS { MODELPARAM_VALUE.SHA3_NUM_ROUNDS PARAM_VALUE.SHA3_NUM_ROUNDS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SHA3_NUM_ROUNDS}] ${MODELPARAM_VALUE.SHA3_NUM_ROUNDS}
}

proc update_MODELPARAM_VALUE.CALC_TROIKA { MODELPARAM_VALUE.CALC_TROIKA PARAM_VALUE.CALC_TROIKA } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CALC_TROIKA}] ${MODELPARAM_VALUE.CALC_TROIKA}
}

proc update_MODELPARAM_VALUE.COLUMNS { MODELPARAM_VALUE.COLUMNS PARAM_VALUE.COLUMNS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.COLUMNS}] ${MODELPARAM_VALUE.COLUMNS}
}

proc update_MODELPARAM_VALUE.ROWS { MODELPARAM_VALUE.ROWS PARAM_VALUE.ROWS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.ROWS}] ${MODELPARAM_VALUE.ROWS}
}

proc update_MODELPARAM_VALUE.SLICES { MODELPARAM_VALUE.SLICES PARAM_VALUE.SLICES } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SLICES}] ${MODELPARAM_VALUE.SLICES}
}

proc update_MODELPARAM_VALUE.SLICESIZE { MODELPARAM_VALUE.SLICESIZE PARAM_VALUE.SLICESIZE } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SLICESIZE}] ${MODELPARAM_VALUE.SLICESIZE}
}

proc update_MODELPARAM_VALUE.STATESIZE { MODELPARAM_VALUE.STATESIZE PARAM_VALUE.STATESIZE } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.STATESIZE}] ${MODELPARAM_VALUE.STATESIZE}
}

proc update_MODELPARAM_VALUE.NUM_SBOXES { MODELPARAM_VALUE.NUM_SBOXES PARAM_VALUE.NUM_SBOXES } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.NUM_SBOXES}] ${MODELPARAM_VALUE.NUM_SBOXES}
}

